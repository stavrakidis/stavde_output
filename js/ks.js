window.onload = function() {
	var links = document.links;

	for (var i = 0, linksLength = links.length; i < linksLength; i++) {
		if (links[i].hostname != window.location.hostname) {
			links[i].target = '_blank';
		} 
	}
}

// Requires jQuery
$(document).ready(function() {
	$('.show-comments').on('click', function(){
		$.ajax({
			type: "GET",
			url: "https://stavrakidis.disqus.com/embed.js",
			dataType: "script",
			cache: true
		}); 
		$(this).fadeOut();
	});
});
